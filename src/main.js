import Vue from 'vue'
import App from './App'
import router from './router'

import BaiduMap from 'vue-baidu-map'

Vue.use(BaiduMap, {
  ak: 'HHIT00bqLzQ22UQmQ54MPQmQnHLzQ54IDKrKCQw0pklKlzQ'
})

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
