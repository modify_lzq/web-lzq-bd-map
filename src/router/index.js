import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Demo from '@/components/Demo'
import Point from '@/components/Point'
import MorePoint from '@/components/MorePoint'
import Line from '@/components/Line'
import Graphical from '@/components/Graphical'
import Circular from '@/components/Circular'
import TileLayer from '@/components/TileLayer'
import LocalSearch from '@/components/LocalSearch'
import LineRoad from '@/components/LineRoad'
import ContextMenu from '@/components/ContextMenu'
import Region from '@/components/Region'

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/demo',
      name: 'Demo',
      component: Demo
    },
    {
      path: '/point',
      name: 'Point',
      component: Point
    },
    {
      path: '/morePoint',
      name: 'MorePoint',
      component: MorePoint
    },
    {
      path: '/line',
      name: 'Line',
      component: Line
    },
    {
      path: '/graphical',
      name: 'Graphical',
      component: Graphical
    },
    {
      path: '/circular',
      name: 'Circular',
      component: Circular
    },
    {
      path: '/tileLayer',
      name: 'TileLayer',
      component: TileLayer
    },
    {
      path: '/localSearch',
      name: 'LocalSearch',
      component: LocalSearch
    },
    {
      path: '/lineRoad',
      name: 'LineRoad',
      component: LineRoad
    },
    {
      path: '/contextMenu',
      name: 'ContextMenu',
      component: ContextMenu
    },
    {
      path: '/region',
      name: 'Region',
      component: Region
    }
  ]
})
