# web-lzq-bd-map

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


# Use Baidu Map

``` bash
npm install vue-baidu-map --save
```

## Code Level Detail

|文件名||说明|
|--|--|--|
|hello||该文件夹代码为原生js代码实现百度地图等
|components|||
||Demo.vue|vue实现百度地图展示及各种控件的添加与展示
||Point.vue|往地图上绘制点
||MorePoint.vue|往地图上绘制大量点
||Line.vue|往地图上绘制线
||Graphical.vue|往地图上绘制多边形
||Circular.vue|往地图上绘制圆
||TileLayer.vue|瓦片图层、交通流量图层
||LocalSearch.vue|地区检索
||LineRoad.vue|公交、步行路线规划
||Region.vue|行政区域划分